import React, { Component } from 'react';
import { Link } from "react-router-dom";
import logo from './assets/main.png';

class Header extends Component {

  constructor(props){
    super(props);
    this.state={
      players:[]
    };
  }

  render(props) {

    let players=[];

    for(let i=0; i<this.props.players.length;i++){
      players.push(<li key={i} className="nav-item" style={{backgroundColor:this.props.players[i].color}}> <span key={i+this.props.players.length} className="nav-link">{this.props.players[i].name}: {this.props.players[i].score} p</span> </li>);
    }
    return <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <a className="navbar-brand" href="/"><img className="main_pic" src={logo} alt="logo" /> Memory Cards</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarColor01">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/">Start</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/play">Play</Link>
          </li>
          {players}
        </ul>
      </div>
    </nav>
    ;
  }
}

export default Header;