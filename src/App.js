import React, { Component } from 'react';
import './App.css';
import Header from './Header';
import Footer from './Footer';
import StartPage from './StartPage';
import GamePage from './GamePage';
import { BrowserRouter, Route } from "react-router-dom";

class App extends Component {

  constructor(props){
    super(props);
    this.state={
      players:[]
    };
  }

  addPlayer = (player) => {
    let players = this.state.players;
    players.push(player);
    this.setState({players:players});
  }

  freshPlayers = (players) =>{
    this.setState({players:players});
  }

  render() {
    return <div className="App">
      <BrowserRouter className="pagelayout">
        <div>
          <Header players={this.state.players}/>
          <Route exact path="/" render={props => <StartPage addPlayer={this.addPlayer} />} />
          <Route path="/play" render={props => <GamePage players={this.state.players} freshPlayers={this.freshPlayers} />} />
          <Route path="/life" />
        </div>
      </BrowserRouter>
      <Footer />
    </div>;
  }
}

export default App;
