import React, { Component } from 'react';

class Colorpicker extends Component {

    handleClick = (e) => {
        e.stopPropagation();
        this.props.handleClick(this.props.color);
    }

    render(props) {
        return <div className="btn colorpicker" style={{background:"linear-gradient(to right bottom,#282c34,"+this.props.color+")"}} onClick={this.handleClick} ></div>;
    }
}

export default Colorpicker;
