import React, { Component } from 'react';
import './Card.css';
import Cards from './CardImages';


class Card extends Component {

  constructor(props) {
    super(props);

    this.state = {
      clickable: true,
      cover: null
    };
  }

  handleClick = (ev) => {
    if (this.state.clickable) {

      if(this.state.cover === null){
        this.setState({ cover: ev.target });        
      }
      
      this.props.handleClick(this);

    }
  }

  hideCard = () => {
    this.state.cover.style.opacity = 1;
  }

  showCard = () => {
    this.state.cover.style.opacity = 0;
  }

  stopEventListener = ()=>{
    this.setState({clickable:false});
  }

  startEventListener = ()=>{
    this.setState({clickable:true});
  }

  render() {

    return <div className="card_container">
      <img className="mycard" src={Cards[this.props.img]} alt="card"/>
      <div id={this.props.index} className="card_overlay" onClick={this.handleClick}></div>
    </div>;
  }
}

export default Card;
