import React, { Component } from 'react';
import './StartPage.css';
//import $ from 'jquery';
import Colorpicker from './Colorpicker';

const colors= ["Tomato", "Orange", "DodgerBlue", "MediumSeaGreen", "Gray", "SlateBlue", "Violet", "LightGray"];

class StartPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      counter:0
    };
  }
  handleClick = (color) => {
    this.setUsersColor(color);
  }

  setUsersColor = (color) =>{
    document.getElementById("usercolor").style.backgroundImage = "none";
    document.getElementById("usercolor").style.backgroundColor = color;
  }

  setUsersColorEmpty = (color) =>{
    document.getElementById("usercolor").style.backgroundColor = "";
    document.getElementById("usercolor").style.backgroundImage = "";

  }

  adduser = () => {
    let name = document.getElementById("username").value;
    let color = document.getElementById("usercolor").style.backgroundColor;

    if(this.state.counter >= colors.length){
      alert("Maximum "+colors.length+" player!")
      return;      
    }
    if(!this.checkUser(name,color)){
      alert("You should fill name, and choose color!")
      return;
    }

    //give player to parent component
    this.setState({counter:this.state.counter+1});
    this.props.addPlayer({name:name,color:color,score:0,id:this.state.counter});
    //reset page
    document.getElementById("usercolor").style.backgroundColor = "";
    document.getElementById("usercolor").style.backgroundImage = "";
    document.getElementById("username").value = "";
  }

  checkUser(name, color){
    if(name===""||color===""){
      return false;
    }
    return true;
  }

  render() {

    let row = [];
    let colorpicker = [];

    for (let i = 0; i < colors.length; i++) {
      colorpicker.push(<Colorpicker color={colors[i]} key={i + "cp"} handleClick={this.handleClick} />);
      if ((i + 1) % 4 === 0) {
        row.push(<div className="colorpicker_container col-md-6" key={(i + 1) / 4 + "cpc"}>{colorpicker}</div>)
        colorpicker = [];
      }
    }

    return <div className="StartPage">
      <h2>Please choose a color, write your name and click "Add player":</h2>
      <div className="row">
        {row}
      </div>

      <div className="adduser">
        <div id="usercolor" className="mb-4 btn userscolor"></div>
        <label htmlFor="username" className="sr-only">Your name</label>
        <input type="email" id="username" className="mb-4 form-control" placeholder="Your name" required="" autoFocus="" />
        <button className="mb-4 btn btn-lg btn-primary btn-block" type="submit" onClick={this.adduser}>Add player</button>
        <button className="mb-4 btn btn-lg btn-primary btn-block" type="submit" onClick={this.setUsersColorEmpty}>Choose another color</button>

      </div>
      <p className="mt-5 mb-3 text-center">To start Game click play on the top</p>
    </div>;
  }
}

export default StartPage;
