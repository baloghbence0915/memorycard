import React, { Component } from 'react';
import Card from './Card';
import './Board.css';

const width = 6, height = 4, grid = 6, cardsInGrid = 3;


class Board extends Component {

    constructor(props) {
        super(props);

        this.state = {
            serials: this.generateSerialNumbers(),
            first_card: null
        }
    }

    getRandomInt = (max) => {
        return Math.floor(Math.random() * Math.floor(max));
    }

    generateSerialNumbers = () => {
        let numOfPairs = (width * height) / 2;
        let serials = [];
        while (serials.length !== numOfPairs * 2) {
            let rnd = this.getRandomInt(numOfPairs);

            let counter = 0;
            for (let i = 0; i < serials.length && counter < 2; i++) {
                if (serials[i] === rnd) {
                    counter++;
                }
            }
            if (counter < 2) {
                serials.push(rnd);
            }
        }

        return serials;
    }

    handleClick = (obj) => {
        if (this.state.first_card === null) {
            this.setState({ first_card: obj });
            setTimeout(() => {
                obj.showCard();
                obj.stopEventListener();
            }, 0);
        } else {
            setTimeout(() => {
                obj.showCard();
                obj.stopEventListener();
            }, 0);

            if (this.state.first_card.props.img === obj.props.img) {
                this.props.addScore();
                this.setState({ first_card: null });
            } else {
                let board = document.getElementById("Board");
                let board_cover = document.createElement('div');
                board_cover.id = "BoardCover";
                board.appendChild(board_cover);

                setTimeout(() => {
                    obj.hideCard();
                    obj.startEventListener();
                    this.state.first_card.hideCard();
                    this.state.first_card.startEventListener();
                    this.setState({ first_card: null });
                    board_cover.parentElement.removeChild(board_cover);
                    this.props.nextPlayer();
                }, 2500);
            }
        }
    }

    generateCards = () => {

        let serial_num = this.state.serials;

        let rows = [];

        let max = height * width;

        for (let i = 0; i < height; i++) {

            let cards = [];
            let grids = [];
            let cell_id = 123456789;

            for (let l = 0; l < width; l++) {
                cell_id = l + (width * i);
                cards.push(<Card index={cell_id} img={serial_num[cell_id]} key={cell_id} handleClick={this.handleClick} />);
                if (cards.length === cardsInGrid) {
                    grids.push(<div className={"cards_container col-lg-" + grid} key={(1 * max) + cell_id}>{cards}</div>);
                    cards = [];
                }
            }

            rows.push(<div className="row" key={(2 * max) + cell_id}>{grids}</div>);
        }

        return rows;


    }

    render() {
        return <div id="Board" className="">
            {this.generateCards()}
        </div>;
    }
}

export default Board;
