import React, { Component } from 'react';
import Board from './Board';
import './GamePage.css';

class GamePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      playerIndex: 0
    };

    setTimeout(() => {
      if (this.props.players[0]) {
        document.getElementsByClassName("App")[0].style.backgroundImage = "linear-gradient(to bottom right,#282c34," + this.props.players[0].color + ")";
      }
    }, 0);


  }



  addScore = () => {
    if (this.props.players[this.state.playerIndex]) {
      this.props.players[this.state.playerIndex].score++;
      this.props.freshPlayers(this.props.players);
    }
  }

  nextPlayer = () => {
    let index = this.state.playerIndex + 1;
    if (index >= this.props.players.length) {
      index = 0;
    }
    this.setState({ playerIndex: index });
    if (this.props.players[index]) {
      document.getElementsByClassName("App")[0].style.backgroundImage = "linear-gradient(to bottom right,#282c34," + this.props.players[index].color + ")";
    }
  }

  render() {
    return <div className="GamePage">
      <Board addScore={this.addScore} nextPlayer={this.nextPlayer} />
    </div>;
  }
}

export default GamePage;
